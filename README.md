# typest

simple typing test that allows user to track their progress

## User Management

Automatically creates or login users by usernames (unique) on login

## Project Structure

backend - Basic Java Springboot application
frontend - Basic CRA (reactJS) application

## Pre-requisites

1. Java17
2. node 20.8.1
3. npm 10.1.0
4. mySQL 8.1.0

## How to run

1. set application.properties mysql db host and credentials
```
    spring.datasource.url=jdbc:mysql://localhost:3306/typest_db
    spring.datasource.username=${dbusername}
    spring.datasource.password=${dbpassword}
```
2. start the backend service from root folder
```
    cd backend/typest && ./mvnw spring-boot:run
```
3. start the frontend service from root folder
```
     cd frontend/typest && npm start
```
4. access the app in localhost:3000

## Demo
https://www.loom.com/share/b3f631b597be49beaddaa83dcddb3266?sid=ae3dca11-2416-4b20-9dd5-145536bf82fb


## TODOs

- [x] Disable copy paste in text area
- [ ] Make reactive character typing
- [ ] Styling :)
- [ ] Login validations and error messages