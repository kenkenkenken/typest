package com.projects.typest.service;

import com.projects.typest.domain.User;

public interface UserService {
    User createOrLogin(String username);
}
