package com.projects.typest.domain;

import jakarta.persistence.*;
import lombok.*;

import java.util.Set;

@Data
@Entity
public class User {

    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE) private Long id;
    @Column(unique=true) private String username;
    @OneToMany(mappedBy = "user") private Set<TypeHistory> typeHistories;
}
