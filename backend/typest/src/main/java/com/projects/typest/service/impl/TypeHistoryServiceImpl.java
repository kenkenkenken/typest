package com.projects.typest.service.impl;

import com.projects.typest.domain.TypeHistory;
import com.projects.typest.domain.User;
import com.projects.typest.repository.TypeHistoryRepository;
import com.projects.typest.service.TypeHistoryService;
import com.projects.typest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
public class TypeHistoryServiceImpl implements TypeHistoryService {

    @Autowired
    private final TypeHistoryRepository repository;

    @Autowired
    private final UserService userService;

    public TypeHistoryServiceImpl(TypeHistoryRepository repository, UserService userService) {
        this.repository = repository;
        this.userService = userService;
    }

    @Override
    public TypeHistory saveTypingHistory(String username, int wordsPerMinute) {
        User user = userService.createOrLogin(username);
        TypeHistory typeHistory = new TypeHistory();
        if (username != null) {
            typeHistory.setUser(user);
        }
        typeHistory.setWordsPerMinute(wordsPerMinute);
        typeHistory.setCreatedAt(new Date());
        return repository.save(typeHistory);
    }

    @Override
    public Set<TypeHistory> getAllTypingHistory(String username) {
        return new HashSet<>(repository.findAllByUser(username));
    }
}
