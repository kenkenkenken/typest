package com.projects.typest.domain;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Data
@Entity
public class TypeHistory {
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE) private Long id;
    private int wordsPerMinute;
    private Date createdAt;
    @ManyToOne private User user;
}
