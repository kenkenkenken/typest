package com.projects.typest.web;

import com.projects.typest.domain.TypeHistory;
import com.projects.typest.service.TypeHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("type-history")
public class TypeHistoryController {

   @Autowired
   private final TypeHistoryService service;

    public TypeHistoryController(TypeHistoryService service) {
        this.service = service;
    }

    @GetMapping()
    public Set<TypeHistory> get(@RequestParam(value = "username", required = false) String username) {
        return service.getAllTypingHistory(username);
    }

    @PostMapping()
    public TypeHistory save(@RequestBody TypeHistory typeHistory){
        return service.saveTypingHistory(typeHistory.getUser().getUsername(), typeHistory.getWordsPerMinute());
    }
}
