package com.projects.typest.service.impl;

import com.projects.typest.domain.User;
import com.projects.typest.repository.UserRepository;
import com.projects.typest.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private final UserRepository repository;

    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public User createOrLogin(String username) {
        if (repository.findByUsername(username) != null) {
            return repository.findByUsername(username);
        }
        User user = new User();
        user.setUsername(username);
        return repository.save(user);
    }
}
