package com.projects.typest.repository;

import com.projects.typest.domain.TypeHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TypeHistoryRepository extends JpaRepository<TypeHistory, Long> {
    @Query(nativeQuery = true,
            value = "SELECT u.id, th.words_per_minute, th.created_at, th.user_id FROM type_history th LEFT JOIN user u ON th.user_id = u.id WHERE u.username = ?1")
    List<TypeHistory> findAllByUser(String username);
}
