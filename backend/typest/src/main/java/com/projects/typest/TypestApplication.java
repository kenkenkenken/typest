package com.projects.typest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TypestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TypestApplication.class, args);
	}

}
