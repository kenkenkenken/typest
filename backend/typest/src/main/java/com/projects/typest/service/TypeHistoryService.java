package com.projects.typest.service;

import com.projects.typest.domain.TypeHistory;
import com.projects.typest.domain.User;

import java.util.Set;

public interface TypeHistoryService {
    TypeHistory saveTypingHistory(String username, int wordsPerMinute);
    Set<TypeHistory> getAllTypingHistory(String username);
}
