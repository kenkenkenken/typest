import './App.css';
import {Route, Routes} from "react-router-dom";
import Login from "./pages/Login";
import TypeTest from "./pages/TypeTest";
import History from "./pages/History";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={ <Login/> } />
        <Route path="type-test" element={ <TypeTest/> } />
        <Route path="history" element={ <History/> } />
      </Routes>
    </div>
  );
}

export default App;
