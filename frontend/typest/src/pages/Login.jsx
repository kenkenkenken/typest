import React, {useState} from 'react'
import {useNavigate} from "react-router-dom";
import axios from "axios";

function Login() {
    const localAPI = "http://localhost:8080";
    const navigate = useNavigate();

    const [username, setUsername] = useState(null);
    const login = (value) => {
        axios.post(localAPI + '/user', {
            username: value
        }).then((response) => {
            navigate("/type-test", { state: { username: response.data.username } });
        });
    };

    return (
        <div>
            <h1>Typestv1</h1>
            <div>
                <input name={'username'} placeholder={'username'} onChange={(e) => setUsername(e.target.value)}/>
            </div>
            <br/>
            <div>
                <button onClick={() => login(username)}>Login</button>
            </div>
        </div>
    )
}

export default Login