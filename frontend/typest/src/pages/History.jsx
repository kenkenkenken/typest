import React, {useEffect, useState} from 'react'
import {useLocation, useNavigate} from "react-router-dom";
import axios from "axios";

function History() {
    const localAPI = "http://localhost:8080";
    const navigate = useNavigate();
    const {state} = useLocation();
    const { username } = state;
    const [histories, setHistories] = useState([]);

    useEffect(() => {
        axios.get(localAPI + '/type-history?username=' + username).then((response) => {
            setHistories(response.data)
        });
    }, []);

    const signout = () => {
        navigate('/');
    };

    const backToTest = () => {
        navigate('/type-test', { state: { username: username } });
    };

    return (
        <div>
            <h1>Typestv1</h1>
            <h2>({username || 'anonymous'})'s history</h2>
            <div>
                {
                    histories.length > 0 ? histories.map(history => (<div key={history.id}>{history.wordsPerMinute} words per minute: {new Date(history.createdAt).toLocaleString("en-US")}</div>))
                    : <h3>No tests taken yet</h3>
                }
            </div>
            <br/>
            <div>
                <button onClick={() => backToTest()}>Back to Test</button>
                <button onClick={() => signout()}>Signout</button>
            </div>
        </div>
    )
}

export default History