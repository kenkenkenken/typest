import React, {useEffect, useState} from 'react'
import {useLocation, useNavigate} from "react-router-dom";
import axios from "axios";

function TypeTest() {
    const phraseAPI = "https://random-word-api.herokuapp.com/word?number=100";
    const localAPI = "http://localhost:8080";

    const {state} = useLocation();
    const navigate = useNavigate();
    const { username } = state;
    const [isWordsLoading, setIsWordsLoading] = useState(true);
    const [words, setPhrase] = useState(null);
    const [typedWords, setTypedWords] = useState(null);

    const [isRunning, setIsRunning] = useState(false);
    const [time, setTime] = useState(0);
    const [wpm, setWPM] = useState(0);
    const minutes = Math.floor((time % 360000) / 6000);
    const seconds = Math.floor((time % 6000) / 100);
    const milliseconds = time % 100;

    const [assessmentIsDone, setAssessmentIsDone] = useState(false);

    useEffect(() => {
        getPhrases();
    }, []);


    useEffect(() => {
        let intervalId;
        if (isRunning) {
            intervalId = setInterval(() => setTime(time + 1), 10);
            if (time > 6000) {
                setIsRunning(false);
                let wordsPerMinute = 0;
                if(typedWords.length > 1) {
                    let ctr = 0;
                    const splitWords = words.split(' ');
                    typedWords.split(' ').forEach((word) => {
                        if (splitWords[ctr] === word) {
                            wordsPerMinute++;
                        }
                        ctr++;
                    })
                }
                setWPM(wordsPerMinute);
                if (username) {
                    axios.post(localAPI + '/type-history', {
                        user: {
                            username: username
                        },
                        wordsPerMinute: wordsPerMinute
                    }).then((response) => {
                        console.log('saved!')
                        setAssessmentIsDone(true)
                    });
                }
            }
        }
        return () => clearInterval(intervalId);
    }, [isRunning, time]);

    const handleTextChange = (value) => {
        setTypedWords(value);
        setIsRunning(true);
    };

    const retry = () => {
        window.location.reload(false);
    };

    const getPhrases = () => {
        setTypedWords(null);
        setIsRunning(false);
        setAssessmentIsDone(false)
        setIsWordsLoading(true);
        axios.get(phraseAPI).then((response) => {
            setPhrase(response.data.join(' '));
            setIsWordsLoading(false);
        });
    };

    const signout = () => {
        navigate('/');
    };

    const getHistory = () => {
        navigate('/history', { state: { username: username } });
    };

    return (
        <div>
            <h1>Typestv1</h1>
            <h2>({username || 'anonymous'})</h2>
            {
                isWordsLoading ?
                    <p>Fetching random words...</p> :
                    <div>
                        {
                            isRunning ? <h2>{minutes.toString().padStart(2, "0")}:{seconds.toString().padStart(2, "0")}:{milliseconds.toString().padStart(2, "0")}</h2>
                                : <h2>Score: {wpm} words per minute</h2>
                        }

                        <p style={{paddingLeft:100,paddingRight:100}}>{words}</p>
                        <p>{typedWords}</p>
                        <textarea style={{width:720,height:300}} placeholder={'Start typing to start the test'}
                        disabled={assessmentIsDone}
                        onPaste={(e) => {
                            e.preventDefault();
                        }}
                        onChange={(e) => handleTextChange(e.target.value)}></textarea>
                    </div>
            }
            <br/>
            <br/>
            <button onClick={() => retry()}>Try Again</button>
            <button onClick={() => getHistory()}>History</button>
            <button onClick={() => signout()}>Sign-out</button>
        </div>
    )
}

export default TypeTest